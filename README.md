# pyporter

#### Description
A rpm packager bot for python modules from pypi.org

#### Installation

1.  python3 setup.py install

#### Preparation
Install below software before using this tool
1.  gcc
2.  gdb
3.  libstdc++-devel
4.  python3-cffi

#### Instructions

pyporter is a tool to create spec file and create rpm for python modules
For more details, please use pyporter -h

pyporter <package> -s -b -d -o python-<package>.spec

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request
